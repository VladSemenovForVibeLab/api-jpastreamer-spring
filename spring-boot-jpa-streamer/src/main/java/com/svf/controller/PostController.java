package com.svf.controller;

import com.speedment.jpastreamer.application.JPAStreamer;
import com.speedment.jpastreamer.streamconfiguration.StreamConfiguration;
import com.svf.data.PostResponse;
import com.svf.model.Post;
import com.svf.model.User;
import com.svf.repository.PostRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class PostController {
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private JPAStreamer jpaStreamer;

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping(value = "/posts", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PostResponse> posts(
            @RequestParam(required = false, defaultValue = "0") long page,
            @RequestParam(required = false, defaultValue = "10") long pageSize
    ) {
        return jpaStreamer
                .stream(Post.class)
                .skip(page + pageSize)
                .limit(pageSize)
                .map(PostResponse::new)
                .collect(Collectors.toList());
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping(value = "/posts/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public PostResponse posts(
            @PathVariable long id
    ) {
        Post post = jpaStreamer
                .stream(Post.class)
                .filter(p -> p.getId() == id)
                .findAny()
                .orElse(null);
        if (post == null) {
            return null;
        }
        return new PostResponse(post);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PostResponse> users() {
        List<Post> posts = jpaStreamer
                .stream(User.class)
                .flatMap(user->user.getPosts().stream())
                .toList();
        return posts.stream()
                .map(PostResponse::new)
                .collect(Collectors.toList());
    }
}
