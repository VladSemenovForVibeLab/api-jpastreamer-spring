package com.svf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJpaStreamerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJpaStreamerApplication.class, args);
	}

}
