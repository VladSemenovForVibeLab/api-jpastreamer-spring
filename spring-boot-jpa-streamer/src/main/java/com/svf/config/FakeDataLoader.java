package com.svf.config;

import com.svf.model.Post;
import com.svf.model.User;
import com.svf.repository.PostRepository;
import com.svf.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
@RequiredArgsConstructor
public class FakeDataLoader {
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    @PostConstruct
    public void loadingData(){
        User user = User
                .builder()
                .username("Vlad")
                .password("1q2w3e")
                .profileImg("image1.jpg")
                .location("Saint Petersburg")
                .createAt(new Date())
                .status("ACT")
                .build();
        userRepository.save(user);
        List<Post> posts = Arrays.asList(
                new Post(1L,"My Post 1",new Date(),user),
                new Post(2L,"My Post 2",new Date(),user),
                new Post(3L,"My Post 3",new Date(),user),
                new Post(4L,"My Post 4",new Date(),user),
                new Post(5L,"My Post 5",new Date(),user),
                new Post(6L,"My Post 6",new Date(),user),
                new Post(7L,"My Post 7",new Date(),user),
                new Post(8L,"My Post 8",new Date(),user),
                new Post(9L,"My Post 9",new Date(),user),
                new Post(10L,"My Post 10",new Date(),user)
        );
        postRepository.saveAllAndFlush(posts);
    }
}
